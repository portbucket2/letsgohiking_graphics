﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectibleBehavior : MonoBehaviour
{
    public GameObject FillCanvas;
    public Image radialBar;
    public float fillAmount;
    public bool fill;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        FillCanvas.transform.rotation = Quaternion.LookRotation(transform.position - Camera.main.transform.position, Vector3.up);
        radialBar.fillAmount = fillAmount;
        if (fill)
        {
            FillIt();
        }
        else
        {
            fillAmount = 0;
        }
        fill = false;
        
    }

    void FillIt()
    {
        if(fillAmount<= 1)
        {
            fillAmount += Time.deltaTime;
        }
    }


}
