﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pathFollowing : MonoBehaviour
{
    
    public GameObject target;
    public float AdvanceSpeed;
    public float RotSpeed;
    public float zFac;
    public int frames;
    public float initialYAngle;
    public float targetPathLength;
    public float tarYRot;

    public float cosA;

    public float finalYRot;
    float journey;
    public int stepsNeeded;
    Quaternion targetRot;

    public float steer;

    public Vector3 steerVector;
    public float directDistance;

    bool GotInitials;
    public bool paused;
    public static pathFollowing instance;
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        CalculateInitials();
    }

    // Update is called once per frame
    void Update()
    {
        //CalculateInitials();
        if (!paused)
        {
            MoveAhead();
        }

        
        //CalculateInitials();
    }


    void MoveAhead()
    {
        if (target)
        {
            this.transform.Translate(0, 0, AdvanceSpeed*Time.deltaTime * 50f);

            
            

            transform.Rotate(0, steer * Time.deltaTime * 50f, 0);
        }
        else
        {
            this.transform.Translate(0, 0, AdvanceSpeed * Time.deltaTime * 50f);
        }
        
    }

    

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "pathPoint")
        {
            target = other.gameObject.GetComponent<pathPointBehavior>().nextPoint.gameObject;
            CalculateInitials();



        }
        
    }


    void CalculateInitials()
    {
        steerVector = transform.InverseTransformDirection(-this.transform.position + target.transform.position);
        directDistance = steerVector.magnitude;

        //stepsNeeded = (int)(directDistance / AdvanceSpeed);

        //steer = steerVector.x * RotSpeed *(1 - directDistance* zFac* Mathf.Abs( steerVector.x ));
        //steer = (steerVector.x / ((directDistance * zFac) * (directDistance * zFac))) * RotSpeed;

        initialYAngle =Mathf.Rad2Deg * Mathf.Atan2((steerVector.x + 0.001f) , steerVector.z);
        //
        cosA = Mathf.Abs(Mathf.Cos ( Mathf.Deg2Rad * (Mathf.Abs(90 - initialYAngle))));
        float radius = (steerVector.magnitude) / (2 * cosA);
        targetPathLength = radius * Mathf.Deg2Rad *(Mathf.Abs(initialYAngle * 2));
        //
        stepsNeeded =(int)Mathf.Abs(targetPathLength / AdvanceSpeed);
        steer  = (initialYAngle* 2) / stepsNeeded;
        //RotSpeed = initialYAngle / stepsNeeded;
        //steerVector = transform.InverseTransformDirection(-this.transform.position + target.transform.position);
        //directDistance = steerVector.magnitude;


    }
}
