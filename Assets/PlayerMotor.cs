﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMotor : MonoBehaviour
{
    public float walkSpeed;
    float walkSpeedApp;
    public float maxRotSpeed;
    float rotSpeedApp;
    public Vector2 trackPos;
    public GameObject camHolder;
    public GameObject camHolderSide;

    Vector2 camRotApp;
    //public float maxcamRot;
    public float clickInY;
    public float clickInX;
    public Vector2 camRotProgress;
    public Vector2 camRotOld;
    public Vector2 camRotNew;

    public float snapSpeed;

    public bool rotDir;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ManageScreenTapPoint();
        ManageCamHolderRot();
        
        ManageWalk();

        FollowPathFollower();





    }

    void ManageScreenTapPoint()
    {
        trackPos.x = (Input.mousePosition.x / Screen.width) - 0.5f;
        trackPos.y = (Input.mousePosition.y / Screen.height);
    }

    void ManageWalk()
    {
        if (Input.GetMouseButton(0))
        {
            pathFollowing.instance.paused = false;
            walkSpeedApp = Mathf.Lerp(walkSpeedApp, walkSpeed, Time.deltaTime * snapSpeed);
            if(Mathf.Abs(trackPos.x) > 0.2f)
            {
                rotSpeedApp = Mathf.Lerp(rotSpeedApp, trackPos.x * maxRotSpeed, Time.deltaTime * snapSpeed);
            }
            else
            {
                rotSpeedApp = Mathf.Lerp(rotSpeedApp, 0, Time.deltaTime * snapSpeed);
            }

            camRotProgress.y = clickInY - trackPos.y;
            camRotProgress.x = clickInX - trackPos.x;

            camRotNew.y = camRotOld.y + camRotProgress.y * 150f;
            camRotNew.x = camRotOld.x+ camRotProgress.x * 100f;

            camRotNew.y = Mathf.Clamp(camRotNew.y, -40f, 40f);
            camRotNew.x = Mathf.Clamp(camRotNew.x, -60f, 60f);
            //camRotApp = camRotNew;


            camRotApp.y = Mathf.Lerp(camRotApp.y, camRotNew.y, Time.deltaTime *snapSpeed);
            camRotApp.x = Mathf.Lerp(camRotApp.x, camRotNew.x, Time.deltaTime * snapSpeed);
        }
        else
        {
            pathFollowing.instance.paused = true;
            walkSpeedApp = Mathf.Lerp(walkSpeedApp, 0, Time.deltaTime * snapSpeed);
            rotSpeedApp = Mathf.Lerp(rotSpeedApp, 0, Time.deltaTime * snapSpeed);
            camRotProgress.y = 0;
            camRotProgress.x = 0;

            camRotApp.y = Mathf.Lerp(camRotApp.y, camRotNew.y, Time.deltaTime * snapSpeed);
            camRotApp.x = Mathf.Lerp(camRotApp.x, camRotNew.x, Time.deltaTime * snapSpeed);
            camRotNew.y = Mathf.Clamp(camRotNew.y, -40f, 40f);
            camRotNew.x = Mathf.Clamp(camRotNew.x, -60f, 60f);

        }

        //transform.Translate(0, 0, walkSpeedApp*(1 - Mathf.Abs(trackPos.x * 2)) * Time.deltaTime * 50f);
        //transform.Translate(0, 0, walkSpeedApp  * Time.deltaTime * 50f);
        if (rotDir)
        {
            transform.Rotate(0, rotSpeedApp * Time.deltaTime * 50f, 0);
        }
        
    }

    void ManageCamHolderRot()
    {
        camHolder.transform.localRotation = Quaternion.Euler(camRotApp.y, 0 ,0);
        camHolderSide.transform.localRotation = Quaternion.Euler(0, -camRotApp.x, 0);
        if (Input.GetMouseButtonDown(0))
        {
            camRotOld = camRotNew;
            clickInY = trackPos.y;
            clickInX = trackPos.x;
        }
        if (Input.GetMouseButtonUp(0))
        {
            
        }
    }

    void FollowPathFollower()
    {
        transform.position =Vector3.Lerp(transform.position, pathFollowing.instance.transform.position , Time.deltaTime * 5);
        transform.rotation = Quaternion.Lerp(transform.rotation, pathFollowing.instance.transform.rotation, Time.deltaTime * 5);
    }

    
}
